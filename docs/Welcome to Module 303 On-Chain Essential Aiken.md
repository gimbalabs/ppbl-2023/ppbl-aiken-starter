# Welcome to Module 303: On-Chain Essential: Aiken

In this module, you will take quite a good dive into this new programming language. It's a functional language like Haskell but is easier to learn and is focused on providing a pleasant developer experience. However, don't be fooled by its simplicity; Aiken is actually very powerful. 

We'll start by showing you the relevant resources to master the language and the basic commands needed to craft smart contracts. Then, you will learn the fundamental concepts and ideas behind the language, followed by a progression of exercises to solidify your knowledge. Finally, we will explore how validators work in Aiken, and you will have some problems to solve as well. As you will see, this module is highly focused on learning by doing.

These are exciting times to learn Cardano development.

Let's get started!

# Lesson 1: Resources and commands. 

## Key resources

In this module, you'll find essential resources to support your learning journey. Expect to consult these resources often during the module:

1. **Aiken Documentation:** This comprehensive guide walks you through the language, covering its features and providing in-depth tutorials. It also offers a clear explanation of EUTxO in the context of smart contract programming.
2. **Standard Library API Documentation:** The basic modules for working with all the types of the language and smart contract development.
3. **Prelude API Documentation:** This resource focuses on the core features of the language.
4. **Built-ins API Documentation:** Discover all the Plutus primitives available for use in Aiken.

While all these resources are valuable, you'll likely find (1) and (2) particularly important for your current learning objectives. 

## The Aiken Playground

If you want to, you can use the the Aiken Playground, its a good place to play with the language without installing anything. 

## Set up: Check if you have the last version installed. 

Check if you have the last released version of Aiken. You can see the last released version [here](https://github.com/aiken-lang/aiken/releases)

```bash
$ aiken --version 
aiken v1.0.16-alpha 1dea348
```

If you haven't the latest version you can upgrade with the following: 

```bash
$ cargo install --git https://github.com/aiken-lang/aiken.git
```

And if for some reason if you don't have Aiken installed, see how to do it in the [installation](https://aiken-lang.org/installation-instructions) section from the docs or see [101.5 module](https://plutuspbl.io/modules/101/1015) .

## Language extension 

Something that will help you a lot is installing the language extension in your code editor, this will show you useful suggestions and compiler messages on live. Now just vs-code, vim and neo-vim are supported, the details are in the end of this [section](https://aiken-lang.org/installation-instructions). 

## Environment & Commands

A quick intro about the most common Aiken commands: **new**, **build** and **check**. Once you have your Aiken installation ready to go, you can start a new project with the  `aiken new` command plus the author name and project name like this: 
```bash
$ aiken new student-name/project-name
```

This will start a new project that will have the following file structure: 

```bash
project-name
├── aiken.toml
├── lib
│   └── project-name
├── README.md
└── validators
```

Let's explain this: 

* **aiken.toml:** This configuration file serves as a project descriptor, detailing the project and listing its dependencies. It plays a crucial role in building validators and managing dependencies.
* **lib/ directory:** Within this directory, you'll store the libraries and modules you use in your project. It's your workspace for organizing reusable code components.
* **validators/ directory:** This directory is where you'll house your validators. As you've noticed, these validator files have the `.ak` extension.
* **README.md**: A read-me file is created automatically.

Unlike older versions, newer version doesn't come with an example validator. We need one to introduce the next commands. Let's copy the dummy contract from the previous module and create a `always-succed.ak` file in the validators folder.

```rust
validator {
  fn always_succeed(_datum: Data, _redeemer: Data, _context: Data) -> Bool {
    True
  }
}
```

Next, execute the `aiken build` command. This command generates a `plutus.json` file, which serves as a specification for your contracts. Inside this file, you'll find a some information, including a contract description, relevant parameters and data types, as well as the compiled code in CBOR format and its corresponding script hash. Take a moment to explore this file and grasp its contents.

In this example, you'll notice that, like all spending validators, the `always succeed` function takes three arguments (or inputs). It unconditionally returns a Boolean value of True, regardless of the parameters provided. Pay attention to the underscores `_` used in the arguments; these instruct the Aiken compiler to disregard them. 

Lastly, there is the `aiken check` command... remove one underscore and execute this command... see what happens! Such a beautiful and annoying compiler message right???! You can use this command to verify the syntax of the code, the type correctness of the functions and execute the unit tests.

## Keep Learning 

Here I leave you some tasks: 

1. **Explore other Aiken commands:** There are some other cool Aiken commands like `docs` or `blueprint`  try to use the `--help` flag and to explore them or others you find interesting. Be curious! 
2. **Study the types and variables section from the maindocumentation:** In the next lesson you will explore some language expressions and features, it will be taken from granted that you have some vague experience with types... but if you don't go to take a look [here](https://aiken-lang.org/language-tour/primitive-types).

 

# Lesson 2: Control flow

![the dude](./the-dude-meme.jpg)

In this lesson you will learn how to be in the good flow and avoid the bad flow. This means have an understanding about how to express the logic of your program, in this case the smart contract. Here we will see the most common ways to manage the logic of your program, but there are more complex features in the documentation (they are less used though). Make sure that you understand the types and variables section of the documentation. 

## Key Concept: Conditions

The key concept of this lesson is the idea of **conditions**: "If this happens, do this...". Plain simple as set conditions to the inputs or parameters that you program takes. We will iterate a lot around this idea in the following. 

## If-else expressions

Here, the conditions are established based on specific truth values that we specify. In this case, these conditions are formulated in expressions that results in Boolean types. 

```rust
1 == 1 		// True
1 == 2 		// False
1 < 2 		// False
```

So after the `if` keyword you set the truth value that you want to set as condition.

```bash
if x > 10 {
	"It's greater than 10."
} else {
	"It's lesser than 10."
}
```

We can add more conditions with the `else if`expression:

```bash
if x > 10 {
	"It's greater than 10."
} else if x > 5 {
	"It's between from 5 to 10"
} else {
    "It's lesser than 5."
}
```

## When-is expression

Here unlike the if-else statements, in a `when is`expression you set as condition a type pattern that you want to match. 

```rust
when some_number is {
  0 -> "Zero"
  1 -> "One"
  2 -> "Two"
  n -> "Some other number" // This matches anything
}
```

Or see this another example: 

```rust
when my_list is {
  [] -> "This list is empty"
  [a] -> "This list has 1 element"
  [a, b] -> "This list has 2 elements"
  _other -> "This list has more than 2 elements"
}
```

So you don't expect a condition a True or False value, just a shape that you expect from a value. It's equivalent to `case` expressions or pattern matching in Haskell. 

## Expect 

A frequently encountered expression that often piques initial curiosity is the `expect` keyword.

```rust
expect [a] = some_list
```

However, it's important to note that this is essentially syntactic sugar of a trivial pattern match: "Expect this value shape, otherwise raise an error." In functional programming, it's a common practice to anticipate a single value match while discarding any other possibilities. 

Also, then you can use that a value in your program: 

```rust
expect [a] = some_list 

if a > 10 {
    "It's greater than 10."
} else {
	"It's lesser than 10."
}
```

## Pipe operator

In functional programming we call **function composition** when you apply a function to the return of another.

```rust
fn double (n: Int) -> Int {
    n * 2
}

fn increment (n: Int) -> Int {
    n + 1
}

// function composition
let result = increment(double(2)) // result == (n * 2) + 1 == 5
```

Here we see another syntactic sugar for this and its the pipe operator, which is equivalent to the previous function composition. 

```rust
let result = double(2) |> increment()
```

Which seems great to avoid some verbose function composition. When you have functions with multi parameters by default the pipe operator assign the result to the first argument, but you can specify the result in different argument order. 

```rust
fn subtraction (x: Int, y: Int) -> Int {
    x - y 
}

let subtraction_1 = 4 |> substraction(2) // subtraction(4,2) = 4 - 2 = 2
let substraction_2 = 4 |> substraction(2,_) // substraction(2,4) = 2 - 4 = -2
```



## The challenge: Exercises

Now, there are exercises for you to practice and achieve a good flow.

Go to the git lab repository and clone the main branch of the [ppbl-aiken-starter](https://gitlab.com/gimbalabs/ppbl-2023/ppbl-aiken-starter), in the validators folder there is an `exercises.ak`file with some problems and tests. 

Run the `aiken check`command inside the project, you will see at the top of the compiler message some test errors. **Your goal is to make these tests pass.** 

With this lesson you should be fine but also you will need to investigate the standard library API documentation. There you will find a lot of functions that will help you, specially those from the list module. Also at the end of the file you will find two exercises that need recursion to be solved. It is up to you to understand this very important concept of functional programming, you will find a lot info in the Internet that will help you. Please, any question don't doubt to bring it to the gimbalabs discord server. 

Lastly, if you need.. you can refer to the `solved-exercises` branch. However, I strongly recommend doing so after giving your best effort to solve the exercises independently first (c'mon you've got this!). Then, after you've completed all the exercises on your own, you can explore the solutions provided by the course instructors to compare and contrast your approach for a deeper learning experience.



# Lesson 3: The Minter validator

Now, once you have acquired some knowledge of this language, let's delve deeper into crafting smart contracts. In this lesson, you will revisit the minter validator from the [203.1 assignment](https://plutuspbl.io/modules/203/assignment2031), but this time it's rewritten in Aiken. You can locate the [implementation](https://gitlab.com/gimbalabs/ppbl-2023/ppbl-aiken-starter/-/blob/main/validators/minter.ak?ref_type=heads) in the validators folder within the **ppbl-aiken-starter** repository. If you are interested, you can also examine the PlutusTx source code [here](https://gitlab.com/gimbalabs/ppbl-2023/ppbl2023-plutus-template/-/blob/main/src/PPBLNFT/Minter.hs). We will explain it step by step, but first, let's have a quick recap of some important concepts.

## Redeemer and script context.

The minting validators receive in their on-chain execution two parameters, the redeemer and the script-context:

1. **Redeemer** is a piece of data provided by the user initiating a transaction. It is used to prove the spending conditions specified in the validator script. For example, if a smart contract requires a certain condition to be met for funds to be spent, the redeemer would contain the necessary proof or data to satisfy that condition.

We will verify that one token is minted, and its Token Name matches the one specified in the redeemer.

2. **Script context** is an interface that provides relevant information about the current transaction. It includes details such as the inputs, outputs, and other contextual information related to the transaction. Script context allows smart contracts to access and process information about the transaction they are a part of, enabling them to make informed decisions and enforce specific rules based on that context. Here is the representation in Aiken: 

```haskell
ScriptContext { transaction: Transaction, purpose: ScriptPurpose }
```

If we go look at the transaction constructor we will see this:  

```haskell
Transaction {
  inputs: List<Input>, 
  reference_inputs: List<Input>,
  outputs: List<Output>,
  fee: Value,
  mint: MintedValue,
  certificates: List<Certificate>,
  withdrawals: Dict<StakeCredential, Int>,
  validity_range: ValidityRange,
  extra_signatories: List<Hash<Blake2b_224, VerificationKey>>,
  redeemers: Dict<ScriptPurpose, Redeemer>,
  datums: Dict<Hash<Blake2b_256, Data>, Data>,
  id: TransactionId,
}
```

There is a lot of useful information here. For this specific minter validator, we'll be inspecting the transaction inputs, the minted value, and the policy ID contained in the purpose constructor. I encourage you to explore the [transaction section](https://aiken-lang.github.io/stdlib/aiken/transaction.html) in the standard library API documentation for more details about the script context.

## Its a parametrized validator

We are already know that, in addition to the datum, redeemer, and script-context, more parameters can be specified to customize a contract. This is the case for our minter validator, where we can apply the following data:

```rust
type PolicyParams {
  contributor: PolicyId,
}
```

We can parameterize the contract directly from Aiken. To do this, we need to execute the following command.

```bash
$ aiken blueprint apply minter.minter ...
```

Woah! An error message! At the end of this lesson there is an exercisome functions that we had to reimplemente for you, you would need to solve that first to this step.

It is worth mentioning that in Aiken this additional parameters are supossed to go next to the `validator`keyword. The other parameters (datum, redeemer or script-context ) go in the inner functions of the validator curly brackets. Like this...

```rust
validator(params: PolicyParams) { // Here you specify the additional parameters
  fn minter(tn: TokenName, ctx: ScriptContext) -> Bool { // Here the rdm and ctx.
      ...
    }
}
```

As a general rule, these inner functions typically have a maximum of 2 to 3 arguments (which corresponds to the datum, redeemer, and script-context). This range may vary depending on whether it's a spending, minting, publishing, or withdrawal validator.

## Smart contract overview

Let's think what our minter validator does: **Essentially, we aim to create an NFT with a name identical to the contributor token, but excluding the 222 prefix.** Below is a simplified diagram illustrating the overall process.

![alt minter.ak](./minter-validator.png)

Now, to the code... First we are going to extract some important information. Here as you see we are using a cool feature of functional languages and its called **deconstructuring:** It's a way of decomposing some variable in parts that we can later use. 

```rust
// Deconstructure the the ScriptContext to get the transaction and purpose field.
let ScriptContext { transaction, purpose } = ctx
// Expect the minting script purpose and get the policy ID by deconstructuring the purpose.
expect tx.Mint(policy_id) = purpose
// Deconstructure the transaction variable to get the inputs and the minted value. 
let Transaction { inputs, mint, .. } = transaction
// Get total value from inputs.
let all_input_values: Value = inputs_value(inputs)
// Then from the previous step, get a list of policyids from the inputs.
let policies: List<PolicyId> = policies(all_input_values)
```

 Then with this information in hand we are going to check these **four conditions**:

1. Verify that the  transaction's input has a **single contributor token** by checking the **policy ID**.

2. Ensure that the **minted amount** is exactly **1**.

3. Validate that the **minted token's name matches** the one specified in **the redeemer.**

4. Confirm that the **redeemer's specified token name matches the PPBL2023 Token without the 222** prefix.


Once these conditions are met, the validator will allow you to mint an NFT with the script policy ID and a token name like this: `PBBL2023<your name>`.

**You will see a more detailed step by step explanation in the video!**

## Exercise: Complete the compare function

At the end of the `validators/minter.ak`file there will be an incomplete function, try to solve it and pass all the unit tests. 

```rust
// Compare the token name without the label. 
fn comparing(a: ByteArray, b: ByteArray) -> Bool {
  todo
}
```

## Keep Learning

* If you want to learn more about smart contract design I highly recommend to checkout the [EUTxO Crash course](https://aiken-lang.org/fundamentals/eutxo) from the the documentation.  
* Nothing in this validator prevents duplicate tokens from being minted in a second or a further transaction. For that we'll need to extend our application a bit. Can you imagine any ways to do so?
* Explore the `blueprint` and `uplc`command... this last one will show you the scripting code that the Plutus virtual machine reads! 



# Lesson 4: The mini Faucet contract

Let's take a look at another interesting contract from a previous lesson: the Mini faucet contract. We'll revisit key concepts related to spending validators, and at the end, you'll find additional exercises based on it. You can find it in the `faucet.ak`file in the `/validators`folder of the repo. 

## Datum and redeemer

The spending validators incorporate one more argument known as the Datum. Initially, the distinction between the redeemer and the datum may seem confusing, but a first to remember is this: **the redeemer relates to the input, while the datum relates to the output (a UTXO).**

1. **Datum**: The Datum is information associated with the output of a transaction (Unspent Transaction Output or UTXO). It serves as a way to provide data or conditions for unlocking or spending the UTXO. Think of it as metadata or requirements attached to the funds you're trying to spend. The Datum ensures that specific conditions are met for the transaction output to be spent.
2. **Redeemer**: So again, the Redeemer, on the other hand, is information provided by the party initiating the transaction. It is used to satisfy the conditions set by the spending validator in order to unlock and spend the UTXO. The Redeemer is related to the input of the transaction and contains the proof that the transaction is authorized. It's essentially the key that fits the lock created by the Datum.

Why do spending validators utilize the datum, while minting validators do not? The distinction lies in their deployment. Minting validators are not permanently deployed on the blockchain; they must be supplied each time they are executed. In contrast, spending validators are deployed by hashing the script to create a script address. Anyone who fulfills the validator's logic gains permission to utilize the locked funds. **Hence, the datum serves as arbitrary data set within the unspent output (UTxO) to assist the validator in achieving this process.** 

## Smart contract overview

The main idea behind this smart contract: Is allow users with the access token to withdraw a certain amount of native tokens. 

Let's explain this piece by piece...

First, the contract is parametrized with two values:

```rust
type FaucetParams {
  access_token_symbol: PolicyId,
  faucet_token_symbol: PolicyId,
}
```

The `access_token_symbol` specifies the policy ID that will give you permissions to withdraw the `faucet_token_symbol`locked in the faucet script address. 

Then you have the datum and redeemer:

```rust
// Specifies the withdrawal token name and amount 
type FaucetDatum {
  withdrawal_amount: Int,
  faucet_token_name: TokenName,
}

// Later used to check if the recepient address also receives the access token.
type FaucetRedeemer {
  sender_pkh: PublicKeyHash,
  access_token_name: TokenName,
}
```

There is some information that the faucet need in order to perform the validation: 

```rust
// Get transaction information from the script-context.
let ScriptContext { transaction, .. } = ctx
// Get a list of tokens's policy's from all the inputs.
let all_tokens: List<PolicyId> =
	inputs_value(transaction.inputs) |> value.policies()

// Get value paid to the receiver.
let Transaction { outputs, .. } = transaction
let value_to_receiver: Value = value_paid_to(outputs, rdm.sender_pkh)

// Find inputs from this script address
let own_input: Input = find_own_input(ctx)

// Find outputs to this script address
expect [own_output] = find_own_outputs(ctx)

// Expect an inline datum from the output to this contract.
expect InlineDatum(data) = own_output.datum

```

 In sum, the faucet needs to know a couple of things: What policy IDs does the inputs have; the value paid to the recipient; the inputs taken from the contract itself; the output that goes to the contract again; the datum that goes to the contract. 

This information is later used to check these **5 conditions**: 

1. Does the input provided has the access token? 
2. Does the output sent to the recipient contains the access token?
3. Does the output sent to the recipient contains the faucet token?
4. Are the remaining funds sent again to the contract?
5. Is the new datum specified the same as the old? 

If these 5 conditions are True the spender will be allowed to do the withdraw. Here is a diagram of a successful transaction.

![faucet diagram](./faucet-validator.png)

For a wrap up and a step by step explanation look at the video.

## Exercise: Complete the missing functions

When we reimplemented this validator to Aiken, we found out that many functions from PlutusTx that facilitate some operations aren't implemented yet. So, we had to reimplement them. Now that you have more experience with the language, it will be a good exercise to reimplement some of them. 

```rust
// Get value paid to a public key hash.
fn value_paid_to(ouputs: List<Output>, receiver: PublicKeyHash) -> Value {
  todo
}

// Get input from the script.
fn find_own_input(ctx: ScriptContext) -> Input {
  todo
}

// Get outputs to the script.
fn find_own_outputs(ctx: ScriptContext) -> List<Output> {
  todo
}

// Get all outputs value, identical to PlutusTx valueSpent function.
fn outputs_value(ouputs: List<Output>) -> Value {
  todo
}
```

You will find of great help to revisit the transaction section from the standard library API documentation. 

# Project: Rewrite the Lockbox validator from module 302.

Cheers for going through so far! Now that you have more experience and some tricks under the sleeve with Aiken, we leave you one final challenge: Rewrite in Aiken of the Lockbox contract from the previous module. While this may seem challenging, we believe you've been sufficiently exposition to the language to accomplish it... all you need is to apply what you have learned in these lessons. You can find the source code of the contract [here](https://gitlab.com/gimbalabs/ppbl-2023/ppbl2023-plutus-template/-/blob/main/src/Lockbox/Validator.hs?ref_type=heads)

## Next steps

If you've enjoyed your experience with Aiken and are eager to delve deeper into it, we have some recommendations:

* **Explore the Comprehensive Aiken Documentation:** The main Aiken documentation offers a wealth of information that goes beyond what we covered in this course. It includes engaging tutorials that span from contract design to creating full-stack applications.

* **Get Involved in the Aiken Project:** As you may have noticed in lesson 303.4, Aiken is an ongoing project with plenty of room for growth and improvement. There are many opportunities to contribute to the language's development. You can help by addressing uncovered topics or details in the documentation, reporting issues you encounter, or even proposing and working on new features. There are many ways to contribute to open source projects like this and you don't have to be an expert to it. In this sense, it is worth mentioning too that the Aiken community and maintainers are very approachable and welcoming.

